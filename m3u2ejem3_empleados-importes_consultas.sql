﻿/* Ejercicio 2 */
  -- crear nueva tabla IMPORTES
  CREATE OR REPLACE TABLE importes(
    tipo varchar(15),
    valor int,
    PRIMARY KEY(tipo)
    );

  -- introducir valores
  INSERT INTO importes (tipo,valor)
    VALUES ('Jornalero',25), ('Efectivo',250)
    ;

/* Ejercicio 3 */
  -- crear campo nuevo SALARIO_BASICO en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN salario_basico;
  ALTER TABLE empleados ADD COLUMN salario_basico float;

  -- actualizacion
   UPDATE empleados e JOIN importes i ON e.Tipo_trabajo=i.tipo
      SET e.salario_basico=
    IF(e.Tipo_trabajo='Jornalero',e.Horas_trabajadas*i.valor,IF(e.Tipo_trabajo='Efectivo',e.Dias_trabajdos*i.valor,0));

  SELECT * FROM empleados e;

/* Ejercicio 4 */
  -- crear campo nuevo PREMIOS en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN premios;
  ALTER TABLE empleados ADD COLUMN premios float;

  -- actualizacion
   UPDATE empleados
      SET premios=salario_basico*
         CASE 
            WHEN (Rubro='Chofer' OR Rubro='Azafata') AND (Dias_trabajdos>20 OR Horas_trabajadas>200) THEN 0.15
            WHEN (NOT(Rubro='Chofer' OR Rubro='Azafata')) AND (Dias_trabajdos>20 OR Horas_trabajadas>200) THEN 0.05
            ELSE 0
         END
          ;
  SELECT * FROM empleados e;

/* Ejercicio 5 */
  -- crear campo nuevo SUELDO NOMINAL en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN sueldo_nominal;
  ALTER TABLE empleados ADD COLUMN sueldo_nominal float;

  -- actualizacion
   UPDATE empleados
      SET sueldo_nominal=salario_basico+premios
          ;
  SELECT * FROM empleados e;

  /* Ejercicio 6 */
  -- crear campo nuevo BPS en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN bps;
  ALTER TABLE empleados ADD COLUMN bps float;

  -- crear campo nuevo IRP en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN irpf;
  ALTER TABLE empleados ADD COLUMN irpf float;

  -- calcular BPS
  UPDATE empleados
  SET bps=sueldo_nominal*0.13;

  -- calcular IRPF
   UPDATE empleados
      SET irpf=
         CASE 
            WHEN sueldo_nominal<4*1160 THEN sueldo_nominal*0.03
            WHEN sueldo_nominal BETWEEN 4*1160 AND 10*1160 THEN sueldo_nominal*0.06
            WHEN sueldo_nominal>10*1160 THEN sueldo_nominal*0.09
         END
          ;
  SELECT * FROM empleados e;

/* Ejercicio 7 */
  -- crear campo nuevo SUBTOTAL_DTOS en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN subtotal_dtos;
  ALTER TABLE empleados ADD COLUMN subtotal_dtos float;

  -- actualizacion
  UPDATE empleados e
  SET e.subtotal_dtos=e.bps+e.irpf;

  SELECT * FROM empleados e;

/* Ejercicio 8 */
  -- crear campo nuevo SUELDO_LIQUIDO en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN sueldo_liquido;
  ALTER TABLE empleados ADD COLUMN sueldo_liquido float;

  -- actualizacion
  UPDATE empleados e
  SET e.sueldo_liquido=e.sueldo_nominal-e.subtotal_dtos;

  SELECT * FROM empleados e;

/* Ejercicio 9 */
  -- crear campo nuevo V-TRANSPORTE en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN vTransporte;
  ALTER TABLE empleados ADD COLUMN vTransporte int;

   -- actualizacion
  UPDATE empleados e
  SET e.vTransporte=
    CASE
        WHEN ((YEAR(NOW())-YEAR(e.`Fecha-nac`))>40 AND NOT(e.Barrio='cordon' OR e.Barrio='centro')) THEN 200
        WHEN ((YEAR(NOW())-YEAR(e.`Fecha-nac`))>40 AND NOT(NOT(e.Barrio='cordon' OR e.Barrio='centro'))) THEN 150
        WHEN ((YEAR(NOW())-YEAR(e.`Fecha-nac`))<40 AND NOT(NOT(e.Barrio='cordon' OR e.Barrio='centro'))) THEN 100
        ELSE 0
    END
  ;

  SELECT * FROM empleados e;


/* Ejercicio 10 */
  -- crear campo nuevo VALIMENTACION en la tabla EMPLEADOS
  ALTER TABLE empleados DROP COLUMN vAlimentacion;
  ALTER TABLE empleados ADD COLUMN vAlimentacion int;

   -- actualizacion
  UPDATE empleados e
  SET e.vAlimentacion=
    CASE
        WHEN e.sueldo_liquido<5000 THEN 300
        WHEN (e.sueldo_liquido BETWEEN 5000 AND 10000) AND e.Tipo_trabajo='Jornalero' THEN 200
        WHEN (e.sueldo_liquido BETWEEN 5000 AND 10000) AND e.Tipo_trabajo='Efectivo' THEN 100
        WHEN e.sueldo_liquido>10000 THEN 0
    END
  ;

  SELECT * FROM empleados e;